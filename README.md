# Documentação

Os artefatos pertinentes ao projeto estão presentes nesse repositório e na [wiki](https://gitlab.com/geminos/eir/docs/wikis/home) do projeto. 

* [Documento Certics](https://gitlab.com/geminos/eir/docs/wikis/documento-certics) - Rastreabilidade entre os artefatos do projeto e o Certics.
* [Processo Simplificado do Projeto](https://gitlab.com/geminos/eir/docs/wikis/processo-simplificado-do-projeto)
* [Termo de Abertura do Projeto](https://gitlab.com/geminos/eir/docs/wikis/Termo-de-Abertura-do-Projeto)
* [Plano de Gerenciamento de Configuração de Software](https://gitlab.com/geminos/eir/docs/wikis/plano-de-gerenciamento-de-configuração-de-software)
* [Plano de Gerenciamento de Projeto](https://gitlab.com/geminos/eir/docs/wikis/Plano-de-Gerenciamento-de-Projeto)
* [Requisitos](https://gitlab.com/geminos/eir/docs/wikis/Requisitos)
   * [Canvas BMG](https://gitlab.com/geminos/eir/docs/wikis/Canvas-BMG)
   * [Canvas VPD](https://gitlab.com/geminos/eir/docs/wikis/canvas-vpd)
   * [Modelo de Domínio do Software](https://gitlab.com/geminos/eir/docs/wikis/modelo-de-dom%C3%ADnio-do-software)
* [Plano Técnico de Desenvolvimento](https://gitlab.com/geminos/eir/docs/wikis/plano-t%C3%A9cnico-de-desenvolvimento)
   * [Design Funcional do Sistema](https://gitlab.com/geminos/eir/docs/wikis/design-funcional-do-sistema)
* [Plano de Treinamento do Projeto](https://gitlab.com/geminos/eir/docs/wikis/plano-de-treinamento-do-projeto)
* [Plano de Suporte do Produto](https://gitlab.com/geminos/eir/docs/wikis/plano-de-suporte-de-produto)
* [Plano e Ações de Mercado](https://gitlab.com/geminos/eir/docs/wikis/plano-e-a%C3%A7%C3%B5es-de-mercado)   
   * [Pesquisa de Mercado](https://gitlab.com/geminos/eir/docs/wikis/pesquisa-de-mercado)   
* [Melhoria Continua do Processo](https://gitlab.com/geminos/eir/docs/wikis/melhoria-continua-de-processo)